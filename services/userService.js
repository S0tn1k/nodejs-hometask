const { UserRepository } = require("../repositories/userRepository");
const { NotFoundError } = require("../config/not-found");

class UserService {
  getAll() {
    return UserRepository.getAll();
  }

  getById(id) {
    const user = UserRepository.getOne({ id });
    if (!user) throw new NotFoundError(`User with id ${id} not found.`);

    return user;
  }

  create(entity) {
    const isNotUniqueEmail = UserRepository.searchOne("email", entity.email);
    const isNotUniquePhoneNumber = UserRepository.getOne({ phoneNumber: entity.phoneNumber });

    if (isNotUniqueEmail) throw new Error("Email should be unique");
    if (isNotUniquePhoneNumber) throw new Error("Phone number should be unique");

    return UserRepository.create(entity);
  }

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  updateById(id, entity) {
    const user = UserRepository.getOne({ id });
    if (!user) throw new NotFoundError(`User with id ${id} not found.`);

    return UserRepository.update(id, entity);
  }

  deleteById(id) {
    const user = UserRepository.getOne({ id });
    if (!user) throw new NotFoundError(`User with id ${id} not found.`);

    return UserRepository.delete(id);
  }
}

module.exports = new UserService();
