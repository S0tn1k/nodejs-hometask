const { Validator } = require("../config/validator");

const loginValid = (req, res, next) => {
  try {
    console.log("hi");
    const requiredFields = ["email", "password"];
    Validator.validate(req.body)
      .haveOnlyAllRequiredFields(...requiredFields)
      .isFieldGmail("email")
      .isFieldString("password");
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
};

exports.loginValid = loginValid;
